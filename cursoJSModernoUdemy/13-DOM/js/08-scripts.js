const navegacion = document.querySelector(".navegacion");
console.log(navegacion);

console.log(navegacion.childNodes);
console.log(navegacion.children);//lista los elementos reales

console.log(navegacion.children[0]);
console.log(navegacion.firstElementChild);
console.log(navegacion.lastElementChild);


const card = document.querySelector(".card");
console.log(card.children[1].children[1].textContent = "nuvo comienzo");

card.children[0].src = "img/hacer2.jpg";


//de hijo a padre
console.log(card.parentElement);

//hermanos
console.log(card.nextElementSibling);

const ultimoCard = document.querySelector(".card:nth-child(4)");
console.log(ultimoCard);
console.log(ultimoCard.previousElementSibling);