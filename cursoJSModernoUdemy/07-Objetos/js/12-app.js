//object literal
const producto = {
    nombre: "Monitor 20 pulgadas",
    precio: 300,
    disponible: true
}

//object constructor
function Producto(nombre, precio){
    this.nombre = nombre;
    this.precio = precio;
    this,disponible = true;
}

const producto2 = new Producto("tablet", 300);
console.log(producto2);

const prod3 = new Producto("televisor", 400);
console.log(prod3);