let total = 0;

function agregarCarrito(precio) {
    return total += precio;
}

function calcularImpuesto(total) {
    return total * 1.15;
}

total = agregarCarrito(300);
const totalPagar = calcularImpuesto(total)
console.log(totalPagar);