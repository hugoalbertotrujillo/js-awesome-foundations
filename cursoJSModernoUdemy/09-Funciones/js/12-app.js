const reproductor = {
    cancion: "",
    reproducir: id => console.log(`Reproduciendo cancion con el id ${id}...`),
    pausando: () => console.log(`pausando...`),
    crearPlaylist: nombre => console.log(`creando la playlist ${nombre}...`),
    reproducirPlaylist: nombre => console.log(`reproduciendo la playlist ${nombre}`),

    set nuevaCancion(cancion) {
        this.cancion = cancion;
        console.log(`añadiendo ${cancion}`);
    },
    get obteniendoCancion(){
        console.log(`${this.cancion}`);
    }

}

reproductor.nuevaCancion = "hasta que el cuerpo aguante";
reproductor.obteniendoCancion;

reproductor.reproducir(30);
reproductor.pausando();

reproductor.borrar = function(id) {
    console.log(`borrando cancion ${id}...`);
}

reproductor.borrar(30);
reproductor.crearPlaylist(`mago_de_oz`);
reproductor.reproducirPlaylist("nirvana");