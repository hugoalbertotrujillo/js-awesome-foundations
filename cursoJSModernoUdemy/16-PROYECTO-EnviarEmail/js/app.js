const btnEnviar = document.querySelector("#enviar");
const btnReset = document.querySelector("#resetBtn");
const email = document.querySelector("#email");
const asunto = document.querySelector("#asunto");
const mensaje = document.querySelector("#mensaje");
const formulario = document.querySelector("#enviar-mail")

const er = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

eventListeners();
function eventListeners(){
     document.addEventListener("DOMContentLoaded", iniciarApp);

     email.addEventListener("blur", validarFormulario);
     asunto.addEventListener("blur", validarFormulario);
     mensaje.addEventListener("blur", validarFormulario);

     formulario.addEventListener("submit", enviarEmail);
     btnReset.addEventListener("click", resetearFormulario);
}

function iniciarApp(){
     console.log("iniciando");
     btnEnviar.disabled = true;
     btnEnviar.classList.add("cursor-not-allowed", "opacity-50");
}

function validarFormulario(e){
    // console.log("validando...");
    // console.log(e.target.value);

    if (e.target.value.length > 0){
        // console.log("si hay algo");

        const error = document.querySelector("p.error");
        if (error){
            error.remove();
        }

        e.target.classList.remove("border", "border-red-500");
        e.target.classList.add("border", "border-green-500");

    } else {
        console.log("no hay nada");
        // e.target.style.borderBottomColor = "red";
        // e.target.classList.add("error");
        e.target.classList.remove("border", "border-green-500");
        e.target.classList.add("border", "border-red-500");
        mostrarError("Todos los campos son obligatorios");
    }

    if (e.target.type === "email"){
        // console.log("es email");
        /*
        const resultado = e.target.value.indexOf("@");
        // console.log(resultado);
        
        if (resultado < 0 ){
            mostrarError("El email no es valido");
        }
        */
       
       
        if (er.test(e.target.value)){
            console.log("Email valido");

            const error = document.querySelector("p.error");
            if (error){
                error.remove();
            }

            e.target.classList.remove("border", "border-red-500");
            e.target.classList.add("border", "border-green-500");
        } else {
            e.target.classList.remove("border", "border-green-500");
            e.target.classList.add("border", "border-red-500");
            mostrarError("El email no es valido");

       }
    }

    if (er.test(email.value) && asunto.value !== "" && mensaje.value !== ""){
        // console.log("pasaste la validacion");
        btnEnviar.disabled = false;
        btnEnviar.classList.remove("cursor-not-allowed", "opacity-50");
    } else {
        console.log("hay campos para validar");
    }
}

function mostrarError(mensaje){
    // console.log("Mostrando error...");
    const mensajeError = document.createElement("p");
    mensajeError.textContent = mensaje;
    mensajeError.classList.add("error", "border", "border-red-500", "background-color-100", "text-red-500", "p-3", "mt-5", "text-center");

    const errores = formulario.querySelectorAll(".error");
    // console.log(errores);
    if (errores.length === 0){

        formulario.appendChild(mensajeError);
    }

    //OTRA MANERA DE VERIFICAR SI EXISTE LA CLASE
    // const errores = formulario.querySelector(".error");
    // if (errores){
    //     return;
    // } else {
    //     formulario.appendChild(mensajeError);
    // }
}

function enviarEmail(e){
    e.preventDefault();
    // console.log("enviando...");

    const spinner = document.querySelector("#spinner");
    spinner.style.display = "flex";

    setTimeout(() => {
        spinner.style.display = "none";

        const parrafo = document.createElement("p");
        parrafo.textContent = "El mensaje se envio correctamente";
        parrafo.classList.add("border", "border-green-500", "background-color-100", "text-green-500", "p-3", "mt-5", "text-center", "mb-5");

        formulario.insertBefore(parrafo, spinner);

        setTimeout(() => {
            parrafo.remove();
            resetearFormulario();
        }, 5000);

    }, 3000);
}

function resetearFormulario(){
    formulario.reset();
    iniciarApp();
}