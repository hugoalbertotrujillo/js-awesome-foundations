function Cliente(nombre, saldo){
    this.nombre = nombre;
    this.saldo = saldo;
};

Cliente.prototype.tipoCliente = function(){
    // console.log("desde mi nuevo prototipe");
    // console.log(this.saldo);

    if (this.saldo > 10000){
        tipo = "Gold";
    } else if (this.saldo > 5000){
        tipo = "platinum";
    } else {
        tipo = "Normal";
    }
    return tipo;
};

Cliente.prototype.nombreClienteSaldo = function(){
    return `Nombre: ${this.nombre}, Saldo: ${this.saldo}, Tipo Cliente: ${this.tipoCliente()}`
}

Cliente.prototype.retiraSaldo = function(retira){
    return this.saldo -= retira;
}

// const Pedro = new Cliente("Pedro", 6000);

// console.log( Pedro.tipoCliente());
// console.log(Pedro.nombreClienteSaldo());
// console.log(Pedro.retiraSaldo(250));
// console.log(Pedro);

function Persona(nombre, saldo, telefono){
    Cliente.call(this, nombre, saldo);
    this.telefono = telefono;
}

Persona.prototype = Object.create(Cliente.prototype);
Persona.prototype.constructor = Cliente;

/**Instanciarlo */
const Juan = new Persona("Juan", 5000, 1234567);
console.log(Juan);
console.log( Juan.nombreClienteSaldo());

Persona.prototype.mostrarTelefono = function(){
    return `El telefono de ${this.nombre} es ${this.telefono}`;
};

console.log(Juan.mostrarTelefono());