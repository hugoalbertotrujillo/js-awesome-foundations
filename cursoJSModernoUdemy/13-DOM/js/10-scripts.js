const enlace = document.createElement("a");
enlace.textContent = "nuevo enlace";
enlace.href = "/nuevo-enlace";
enlace.target = "_blank";
enlace.setAttribute("data- enlace")
enlace.classList.add("nueva-clase");
enlace.onclick = miFuncion;
console.log(enlace);

const navegacion = document.querySelector(".navegacion");
navegacion.appendChild(enlace) //lo pone en ultimo lugar
console.log(navegacion);

console.log(navegacion.children[1]);
navegacion.insertBefore(enlace, navegacion.children[1]);

function miFuncion(){
    alert("diste click");
};


//CREANDO UN CADR

const parrafo1 = document.createElement("p");
parrafo1.textContent = "Concierto";
parrafo1.classList.add("categoria", "concierto");

console.log(parrafo1);

const parrafo2 = document.createElement("p");
parrafo2.textContent = "Concierto de rock";
parrafo2.classList.add("titulo");
console.log(parrafo2);

const parrafo3 = document.createElement("p");
parrafo3.textContent = "800 por persona";
parrafo3.classList.add("precio");
console.log(parrafo3);

const info = document.createElement("div");
info.classList.add("info")

info.appendChild(parrafo1)
info.appendChild(parrafo2)
info.appendChild(parrafo3)
console.log(info);

const imagen = document.createElement("img");
imagen.src = "img/hacer2.jpg";
imagen.alt = "texto alternativo"
console.log(imagen);

const card = document.createElement("div");
card.classList.add("card");
card.appendChild(imagen);
card.appendChild(info);
console.log(card);

const contenedor = document.querySelector(".contenedor-cards");
contenedor.appendChild(card);
contenedor.insertBefore(card, contenedor.children[0])