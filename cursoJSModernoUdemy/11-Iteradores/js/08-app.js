const pendientes = ["tarea", "comer", "proyecto","estudiar"];

const automovil = {
    modelo: "Camaro",
    year: 1969,
    motor: "6.0"
}

for (let dato in automovil){
    console.log(dato);
}

for (let dato in automovil){
    console.log(automovil[dato]);
}

for (let [llave, valor] of Object.entries(automovil)){
    console.log(llave, valor);
}