const carrito = [
    {nombre: "Monitor 20 pulgadas", precio: 500},
    {nombre: "Television", precio: 100},
    {nombre: "Tablet", precio: 200},
    {nombre: "Audifonos", precio: 300},
    {nombre: "Teclado", precio: 400},
    {nombre: "Celular", precio: 700},
];

const pendientes = ["tarea", "comer", "proyecto","estudiar"];

for (let pendiente of pendientes) {
    console.log(pendiente);
};

for (let objeto of carrito){
    console.log(objeto.nombre);
}