const meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio'];
const meses2 = ["Agosto", "Septiembre", "Octubre"];
const meses3 = ["Noviembre", "Diciembre"];

const carrito = [
    { nombre: 'Monitor 27 Pulgadas', precio: 500 },
    { nombre: 'Televisión', precio: 100 },
    { nombre: 'Tablet', precio: 200 },
    { nombre: 'Audifonos', precio: 300 },
    { nombre: 'Teclado', precio: 400 },
    { nombre: 'Celular', precio: 700 },
]

let res = [...meses, "Agosto"];
console.log(res);

const producto = {producto: "Disco Duro", precio: 300};

let res2 = [...carrito, producto];
console.log(res2);