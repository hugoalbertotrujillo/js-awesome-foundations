const btnFlotante = document.querySelector(".btn-flotante");
const footer = document.querySelector(".footer");

btnFlotante.addEventListener("click", mostrarOcultarFooter);

function mostrarOcultarFooter(){
    //console.log("diste click en el boton");
    if (footer.classList.contains("activo")){
        footer.classList.remove("activo");
        this.classList.remove("activo");
        this.textContent = "Idioma y moneda";
    } else {
        footer.classList.add("activo");
        this.classList.add("activo");
        this.textContent = "X cerrar";
        //el this hace referencia al btnFlotante, por que la funcion hace parte del 
        //contexto del boton
    }
}