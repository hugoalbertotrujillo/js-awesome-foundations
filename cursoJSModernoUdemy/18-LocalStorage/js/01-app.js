/*solo se almacenan datos tipo string */
localStorage.setItem("nombre", "hache");

sessionStorage.setItem("apellido", "tresComaCatorce");

const producto = {
    monitor: "20 pulgadas",
    precio: 300
}

/**entonces a los objetos y a los arrays se los vuelve string para poder anexarlos al local storage */
const productoString = JSON.stringify(producto);
localStorage.setItem("objeto", productoString);

const meses = ["enero", "febrero", "marzo"];
const mesesString = JSON.stringify(meses);
localStorage.setItem("array", mesesString);