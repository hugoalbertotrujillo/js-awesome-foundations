const producto = {
    nombre: "Monitor 20 pulgadas",
    precio: 300,
    disponible: true
}

const numeros = [10, 20, 30, 40, 50, 60, 70];

const [uno, , tres, ...cuatro] = numeros;
console.log(tres);
console.log(cuatro);