"use strict";

const producto = {
    nombre: "Monitor 20 pulgadas",
    precio: 300,
    disponible: true
}

Object.seal(producto);
console.log(Object.isSealed(producto));

//aqui lo unico que se puede hacer es modificar lo existente
producto.precio = 100;
console.log(producto);