const cliente = {
    nombre: "Juan",
    saldo: 500
}

console.log(cliente);

function Cliente(nombre, saldo){
    this.nombre = nombre;
    this.saldo = saldo;
}

const Juan = new Cliente("Jota", 500);
console.log(Juan);

console.log(typeof cliente, typeof Juan);