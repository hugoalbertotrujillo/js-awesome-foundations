function Cliente(nombre, saldo){
    this.nombre = nombre;
    this.saldo = saldo;
};

Cliente.prototype.tipoCliente = function(){
    // console.log("desde mi nuevo prototipe");
    // console.log(this.saldo);

    if (this.saldo > 10000){
        tipo = "Gold";
    } else if (this.saldo > 5000){
        tipo = "platinum";
    } else {
        tipo = "Normal";
    }
    return tipo;
};

Cliente.prototype.nombreClienteSaldo = function(){
    return `Nombre: ${this.nombre}, Saldo: ${this.saldo}, Tipo Cliente: ${this.tipoCliente()}`
}

Cliente.prototype.retiraSaldo = function(retira){
    return this.saldo -= retira;
}

const Pedro = new Cliente("Pedro", 6000);
console.log( Pedro.tipoCliente());
console.log(Pedro.nombreClienteSaldo());
console.log(Pedro.retiraSaldo(250));
console.log(Pedro);


function Empresa(nombre, saldo, categoria){
    this.nombre = nombre;
    this.saldo = saldo;
    this.categoria = categoria;
}

const CCJ = new Empresa("codigo con juan", 4000, "cursos en linea");
console.log(CCJ);