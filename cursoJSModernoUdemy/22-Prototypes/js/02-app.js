function Cliente(nombre, saldo){
    this.nombre = nombre;
    this.saldo = saldo;
}

const Juan = new Cliente("Jota", 500);

function formatearCliente(cliente){
    const {nombre, saldo} = cliente;
    return `El cliente ${nombre} tiene un saldo de ${saldo}`
}

console.log(formatearCliente(Juan));

function Empresa(nombre, saldo, categoria){
    this.nombre = nombre;
    this.saldo = saldo;
    this.categoria = categoria;
}

const CCJ = new Empresa("codigo con juan", 4000, "cursos en linea");
console.log(formatearEmpresa(CCJ));

function formatearEmpresa(empresa){
    const {nombre, saldo, categoria} = empresa;
    return `La empresa ${nombre} tiene un saldo de ${saldo} y pertenece a la categoria ${categoria}`
}