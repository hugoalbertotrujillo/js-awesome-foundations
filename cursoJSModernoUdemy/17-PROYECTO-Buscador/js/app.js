const resultado = document.querySelector("#resultado");

const marca = document.querySelector("#marca");
const modelo = document.querySelector("#modelo");
const year = document.querySelector("#year");
const precioMin = document.querySelector("#minimo");
const precioMax = document.querySelector("#maximo");
const puertas = document.querySelector("#puertas");
const transmision = document.querySelector("#transmision");
const color = document.querySelector("#color");

const max = new Date().getFullYear();
const min = max - 10;

const datosBusqueda = {
    marca: "", 
    year: "", 
    precioMin: "",
    precioMax: "",
    puertas: "", 
    transmision: "",
    color: ""
}

document.addEventListener("DOMContentLoaded", () => {
    mostrarAutos(autos);
    llenarSelect();
});

marca.addEventListener("change", e => {
    console.log(e.target.value);
    datosBusqueda.marca = e.target.value;

    console.log(datosBusqueda);
    filtrarAuto();
});

// modelo.addEventListener("change", e => {
//     console.log(e.target.value);
//     datosBusqueda.modelo = ;

//     console.log(datosBusqueda);
// });

year.addEventListener("change", e => {
    console.log(e.target.value);
    datosBusqueda.year = e.target.value;

    console.log(datosBusqueda);
    filtrarAuto();
});

precioMin.addEventListener("change", e => {
    console.log(e.target.value);
    datosBusqueda.precioMin = e.target.value;

    console.log(datosBusqueda);
    filtrarAuto()
});

precioMax.addEventListener("change", e => {
    console.log(e.target.value);
    datosBusqueda.precioMax = e.target.value;

    console.log(datosBusqueda);
    filtrarAuto();
});

puertas.addEventListener("change", e => {
    console.log(e.target.value);
    datosBusqueda.puertas = e.target.value;

    console.log(datosBusqueda);
    filtrarAuto();
});

transmision.addEventListener("change", e => {
    console.log(e.target.value);
    datosBusqueda.transmision = e.target.value;

    console.log(datosBusqueda);
    filtrarAuto();
});

color.addEventListener("change", e => {
    console.log(e.target.value);
    datosBusqueda.color = e.target.value;

    console.log(datosBusqueda);
    filtrarAuto();
});


function mostrarAutos(autos){
    // console.log("mostrar autos...");
    limpiarHTML();

    if (autos.length > 0){
        autos.forEach( auto => {
            const autoHTML = document.createElement("p")
            const {marca, modelo ,year, precio, puertas, color, transmision} = auto;
            autoHTML.textContent = `
                ${marca} ${modelo} - ${year} - Precio: ${precio} - ${puertas} Puertas - Color: ${color} - Trasmision: ${transmision}
            `
            resultado.appendChild(autoHTML);
        });
    } else {
        const texto = document.createElement("p");
        texto.textContent = "No hay coincidencias con tu busqueda";
        texto.classList.add("alerta", "error");
        resultado.appendChild(texto);
    }
}

function limpiarHTML(){
    while(resultado.firstChild){
        resultado.firstChild.remove();
    }
}


function llenarSelect(){
    console.log('llenando...');
    for(let i = max; i >= min; i--){
        const opcion = document.createElement("option");
        opcion.value = i;
        opcion.textContent = i;
        year.appendChild(opcion);
    }
}

function filtrarAuto(){
    // console.log("filtrando...");
    const resultado = autos.filter( filtrarMarca).filter(filtrarYear).filter(filtrarMinimo).filter(filtrarMaximo).filter(filtrarPuertas).filter(filtrarTrasmision).filter(filtrarColor);
    console.log(resultado);

    mostrarAutos(resultado);

}

function filtrarMarca(auto){
    // console.log(auto.marca);
    if (datosBusqueda.marca){
        return auto.marca === datosBusqueda.marca
    }
    return auto;
}

function filtrarYear(auto ){
    if (datosBusqueda.year){
        return auto.year === parseInt(datosBusqueda.year)
    }
    return auto;
}

function filtrarMinimo(auto ){
    if (datosBusqueda.precioMin){
        return auto.precio >= parseInt(datosBusqueda.precioMin)
    }
    return auto;
}

function filtrarMaximo(auto ){
    if (datosBusqueda.precioMax){
        return auto.precio <= parseInt(datosBusqueda.precioMax)
    }
    return auto;
}

function filtrarPuertas(auto ){
    if (datosBusqueda.puertas){
        return auto.puertas === parseInt(datosBusqueda.puertas)
    }
    return auto;
}

function filtrarTrasmision(auto ){
    if (datosBusqueda.transmision){
        return auto.transmision === datosBusqueda.transmision
    }
    return auto;
}

function filtrarColor(auto ){
    if (datosBusqueda.color){
        return auto.color === datosBusqueda.color
    }
    return auto;
}