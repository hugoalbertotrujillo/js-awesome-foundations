//variables
const formulario = document.querySelector("#formulario");
const listaTweets = document.querySelector("#lista-tweets");

let tweets = [];

//Event Listeners
eventListeners();
function eventListeners(){
    //cuando el usuario agrega un nuevo tweet
    formulario.addEventListener("submit", agregarTweet);

    //cuando el documeto esta listo
    document.addEventListener("DOMContentLoaded", () => {
        tweets = JSON.parse(localStorage.getItem("tweets")) || [];
        console.log(tweets);
        crearHTML(tweets);
    });
}


//funciones

function agregarTweet(e){
    e.preventDefault();

    // console.log("agregando tweet...");

    //textarea donde el usuario escribe
    const tweet = document.querySelector("#tweet").value;
    // console.log(tweet);
    
    //validacion
    if(tweet === ""){
        // console.log("NO puede ir vacio");
        mostrarError("el tweet NO puede ir vacio");
        return; //evita que se ejecuten mas lineas de codigo
    }
    // console.log("Agregando tweet");

    const tweetObj = {
        id: Date.now(),
        tweet
    };

    // Añadir el arreglo de tweets
    tweets = [...tweets, tweetObj];
    console.log(tweets);
    // UNa vez agregado poner el HTML
    crearHTML(tweets);


    //Reiniciar el formulario
    formulario.reset();
}

function mostrarError(mensaje){
    const mensajeError = document.createElement("p");
    mensajeError.textContent = mensaje;
    mensajeError.classList.add("error");

    // Insertarlo en el contenido
    const contenido = document.querySelector("#contenido");
    contenido.appendChild(mensajeError);

    setTimeout(() => {
        contenido.removeChild(mensajeError);
    }, 3000);
};

//Muestra un listado de los tweets
function crearHTML(tweets){
    
    limpiarHTML();

    if (tweets.length > 0){
        tweets.forEach( tweet => {
            //agregar boton de eliminar
            const btnEliminar = document.createElement("a");
            btnEliminar.classList.add("borrar-tweet");
            btnEliminar.textContent = "X";

            //Crear el HTML
            const li = document.createElement("li");
            li.innerText = tweet.tweet;
            
            //asignar el boton
            li.appendChild(btnEliminar);
            
            //añadir la funcion de eliminar
            btnEliminar.onclick = () => {
                borrarTweet(tweet.id);
            };

            listaTweets.appendChild(li);
            // listaTweets.innerHTML = `
            // <p>${tweet.tweet}</p>
            // `
        });
    };
    
    sincronizarStorage();
};

//agrega los tweets actuales a localStorage
function sincronizarStorage(){
    localStorage.setItem("tweets", JSON.stringify(tweets))
}

//Limpiar el codigo
function limpiarHTML(){
    while(listaTweets.firstChild){
        listaTweets.removeChild(listaTweets.firstChild);
    }
};

function borrarTweet(id){
     console.log(id);
     tweets = tweets.filter( tweet => tweet.id !== id);
     crearHTML(tweets);
}