const reproductor = {
    reproducir: function(id){
        console.log(`Reproduciendo cancion con el id ${id}...`);
    },
    pausando: function(){
        console.log(`pausando...`);
    },
    crearPlaylist: function(nombre){
        console.log(`creando la playlist ${nombre}...`);
    },
    reproducirPlaylist: function(nombre){
        console.log(`reproduciendo la playlist ${nombre}`);
    }
}

reproductor.reproducir(30);
reproductor.pausando();

reproductor.borrar = function(id) {
    console.log(`borrando cancion ${id}...`);
}

reproductor.borrar(30);
reproductor.crearPlaylist(`mago_de_oz`);
reproductor.reproducirPlaylist("nirvana");