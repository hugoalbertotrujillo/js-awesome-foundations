const carrito = document.querySelector("#carrito");
const contenedorCarrito = document.querySelector("#lista-carrito tbody");
const vaciarCarritoBtn = document.querySelector("#vaciar-carrito");
const listaCursos = document.querySelector("#lista-cursos");
let articulosCarrito = [];

cargarEventListeners();
function cargarEventListeners(){
    listaCursos.addEventListener("click", agregarCurso);
    carrito.addEventListener("click", eliminarCurso);
    vaciarCarritoBtn.addEventListener("click", vaciarCarrito);
}

function agregarCurso(e){
    // console.log(e.target.classList);
    e.preventDefault()
    if (e.target.classList.contains("agregar-carrito")){
        // console.log("agregarCurso");
        // console.log(e.target.parentElement.parentElement);
        const cursoSeleccionado = e.target.parentElement.parentElement;
        leerDatosCurso(cursoSeleccionado);
    }
}

function leerDatosCurso(curso){
    // console.log(curso);

    infoCurso = {
        imagen: curso.querySelector("img").src,
        titulo: curso.querySelector("h4").textContent,
        precio: curso.querySelector(".precio span").textContent,
        id: curso.querySelector("a").getAttribute("data-id"),
        // id: curso.querySelector("a").dataset.id ESTA ES OTRA MANERA DE TRAER EL DATO DEL DATA ATTIBUTE
        cantidad: 1
    }

    const existe = articulosCarrito.some( curso => curso.id === infoCurso.id);
    if (existe){
     //actualizar la cantidad
     const cursos = articulosCarrito.map( curso => {
         if (curso.id === infoCurso.id){
             curso.cantidad++;
             return curso;
         } else {
             return curso;
         }
        });

        articulosCarrito = [...cursos];
    } else {
        //agregar al carrito
        articulosCarrito = [...articulosCarrito, infoCurso];
    }


    // console.log(infoCurso);
    // console.log(articulosCarrito);
    carritoHTML()
}

function carritoHTML(){
    limpiarHTML();
    articulosCarrito.forEach( curso => {
        const row = document.createElement("tr");
        const {imagen, titulo, precio, id, cantidad} = curso;
        row.innerHTML = `
            <td> <img src="${imagen}"width="100px"> </td>
            <td> ${titulo} </td>
            <td> ${precio} </td>
            <td> ${cantidad} </td>
            <td> <a href="#" class="borrar-curso" data-id=${id}> X </td>
        `;

        contenedorCarrito.appendChild(row);
    });
}

function limpiarHTML(){
    // contenedorCarrito.innerHTML = ``
    while(contenedorCarrito.firstChild){
        contenedorCarrito.firstChild.remove();
    }
}

function eliminarCurso(e){
    // console.log(e.target);
    if (e.target.classList.contains("borrar-curso")){
        //  console.log(e.target.getAttribute("data-id"));
        const cursos = articulosCarrito.filter( curso => {
            return curso.id !== e.target.getAttribute("data-id")
        });
        articulosCarrito = [...cursos]
        carritoHTML()
    }
}

function vaciarCarrito(e){
    // console.log(e.target.getAttribute("id"));
    // if (e.target.getAttribute("id")){
    //     articulosCarrito = [];
    //     carritoHTML();
    // }
    articulosCarrito = [];
        carritoHTML();
}